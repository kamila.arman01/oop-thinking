public class Product {

    private int Id;
    private int price;
    private String name;

    Product(int id, int price, String name) {
        this.Id = id;
        this.price = price;
        this.name = name;
    }

    public void removeProduct(){

    }

    public int getId() {
        return Id;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }
}
