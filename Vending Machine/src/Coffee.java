public class Coffee extends Drink{
    private boolean whippedCream;
    private boolean milk;
    Coffee(int id, int price, String name) {
        super(id, price, name);

    }

    public void setWhippedCream(boolean whippedCream) {
        this.whippedCream = whippedCream;
    }

    public void setMilk(boolean milk) {
        this.milk = milk;
    }
}
