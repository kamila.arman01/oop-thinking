public interface Display {
    public void listOfProducts();
    public void prepareGlass();
    public void selectProduct(int selProdId);
    public void enterCoinsMsg();
    public void dispenceProduct();
}
