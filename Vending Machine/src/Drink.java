public class Drink extends Product{
    private String sizeOfGlass;

    Drink(int id, int price, String name) {
        super(id, price, name);

    }

    public void setSizeOfGlass(String sizeOfGlass) {
        this.sizeOfGlass = sizeOfGlass;
    }

}
